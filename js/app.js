//------------------------------ random ------------------------------
console.log('RANDOM:');

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

var randNumberFunc = function(start, end) {
    var randNumber = 0;
    var flag;
    if (isNumeric(start) && isNumeric(end)) {
        if (start > end) {
            flag = start;
            start = end;
            end = flag;
        }
        randNumber = Math.floor(Math.random() * (end - start + 1)) + start;
        console.log(randNumber + ' - both');
    } else if (isNumeric(start)) {
        randNumber = Math.floor(Math.random() * (Number.MAX_SAFE_INTEGER - start + 1)) + start;
        console.log(randNumber + ' - just from start');
    } else if (isNumeric(end)) {
        randNumber = Math.floor(Math.random() * (end - Number.MIN_SAFE_INTEGER + 1)) + Number.MIN_SAFE_INTEGER;
        console.log(randNumber + ' - just to end');
    } else {
        console.log('Sorry, I dont know what I can do');
    }
    return randNumber;
};

randNumberFunc(null, 100);
randNumberFunc(100, null);
randNumberFunc(0, 100);
randNumberFunc(110, 100);
randNumberFunc('bdf', undefined);

//------------------------------ drobi ------------------------------
console.log('DROBI:');

var getDecimal = function(num) {
    if (!isNumeric(num)) {
        console.log('Sorry, its not a number');
        return;
    }
    var drob = '' + num;
    var index = drob.indexOf('.');
    if (index != -1) {
        drob = drob.slice(index);
    }
    console.log(drob + ' - part after dot');
    return +drob;
};

getDecimal(421.37528725);

//------------------------------ truncate ------------------------------
console.log('TRUNCATE:');

var truncate = function(str, maxlength) {
    if (str.length > maxlength) {
        str = str.slice(0, maxlength + 1);
    }
    console.log(str + String.fromCharCode(8230));
    return str + String.fromCharCode(8230);
};

str = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, id est laborum.';

truncate(str, 20);

//------------------------------ addClass ------------------------------
console.log('AddClass:');

var addClass = function(className, newClass) {
    var classes;
    if (className.length != 0) {
        classes = className.split(' ');
    }
    for (var i = 0; i < classes.length; i++) {
        if (classes[i] == newClass) {
            console.log('This class is exist');
            return;
        }
    }
    classes.push(newClass);
    className = classes.join(' ');
    console.log(className);
    return className;
};

var className = 'fa-icon';

addClass(className, 'fa');

//------------------------------ deepClone ------------------------------
console.log('deepClone OBJ:');

var newCat = {
    Name: 'Murzik',
    color: 'black',
    friend: 'Vasya',
    ration: {
        breakfest: 'chiken',
        lunch: 'beef',
        dinner: 'pork',
    }
};

var deepCountProperties = function deepCount(obj) {
    var cloneObject = {};
    for (var key in obj) {
        if (typeof obj[key] === 'object') {
            alert(key);
            cloneObject[key] = deepCount(obj[key]);
        } else {
            alert(key + ': ' + obj[key]);
            cloneObject[key] = obj[key];
        }
    }
    return cloneObject;
};

// deepCountProperties(newCat);

//------------------------------ mySplice ------------------------------
console.log('MY SPLICE:');

Array.prototype.mySplice = function(start, deleteCount) {

    // если значение начала больше, то просто вставляем в конец элементы, если будут
    if (start > this.length) {
        for (var i = 2; i < arguments.length; i++) {
            this.push(arguments[i]);
        }
        return []; // возвращаем пустой
    }

    var flagArray = [];
    var removedArray = [];
    // если начало отрицательное, то меняем его на положительный (будет с конца)
    if (start < 0) {
        start = this.length + start;
    }
    // заносим в промежуточный массив элементы с начала до позиции, на которой будем отрезать
    for (var j = 0; j < start; j++) {
        flagArray[j] = this[j];
    }
    // запоминаем в другой промежуточный массив элементы, которые будут вырезаны
    for (j = start; j < ((start + deleteCount) >= this.length ? this.length : (start + deleteCount)); j++) {
        // if (j >= this.length) {
        //     break;
        // }
        removedArray.push(this[j]);
    }
    // в промежуточный добавляем новые элементы на место старых
    for (j = 2; j < arguments.length; j++) {
        flagArray.push(arguments[j]);
    }
    // промежуточный дополняем оставшимися элементами
    for (j = start + deleteCount; j < this.length; j++) {
        flagArray.push(this[j]);
    }
    // перезаписываем наш массив
    this.length = 0;
    for (j = 0; j < flagArray.length; j++) {
        this[j] = flagArray[j];
    }

    return removedArray;
};

var arr = ['ангел', 'клоун', 'мандарин', 'хирург', 'человек', 'юг', 'ветер'];

var removed = arr.mySplice(4, 2, 'ANIMAL');
console.log(removed);
console.log(arr);

//------------------------------ SHAKE array ------------------------------
console.log('SHAKE ARRAY:');

var arrForShake = [2, 4, 24, 32, 3, 113, 4, 4, 24, 32, 3, 113, 4];

var shake = function(arr) {
    var newArr = [];
    var randPos = 0;
    var amountElemets = arr.length;

    while (amountElemets > 0) {
        randPos = Math.floor(Math.random() * amountElemets);
        newArr.push(arr[randPos]);
        arr.splice(randPos, 1);
        amountElemets--;
    }

    return newArr;
};

console.log(arrForShake);
console.log(shake(arrForShake));

//------------------------------ DATE ------------------------------
console.log('START of MONTH:');

var startOfMonth = function(date) {
    var yy;
    var mm;
    var dd = '01';

    if (date instanceof Date || (typeof date == 'number' && isNumeric(date))) {
        date = new Date(date);
        date = new Date(date);

        yy = date.getFullYear();
        mm = ('0' + (date.getMonth() + 1)).slice(-2);

    } else if (typeof date == 'string') {
        mm = date.slice(3, 5);
        yy = date.slice(6, 10);
    }


    return dd + '/' + mm + '/' + yy;
};

console.log(startOfMonth(new Date()));
console.log(startOfMonth(Date.now()));
console.log(startOfMonth('19/09/2016'));
console.log(startOfMonth(523553232532.523523));

//------------------------------ GO(KM) ------------------------------
console.log('GO(KM):');

function goWrapper() {
    var miliage = 0;

    return function go(km) {
        return miliage += km;
    };
};

var go = goWrapper();

console.log(go(5));
console.log(go(20));
console.log(go(4));